class CreateMagazines < ActiveRecord::Migration
  def change
    create_table :magazines do |t|
      t.string :title
      t.references :user, index: true, foreign_key: true
      t.timestamps null: true
    end
  end
end
