class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title      
      t.string :author
      t.integer :magazine_id
 
      t.timestamps null: true
    end
  end
end
