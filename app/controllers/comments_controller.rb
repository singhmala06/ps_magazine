class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :edit, :update, :destroy]


  #def index
   # @comments = Comment.all
   # respond_with(@comments)
  #end

  #def show
   # respond_with(@comment)
  #end

  def new
    @comment = Comment.new(:parent_id => params[:parent_id],:article_id => params[:article_id])
    #@comment = Comment.new
    #respond_with(@comment)
  end

  #def edit
  #end

  def create
    @comment = current_user.comments.new(comment_params)
    respond_to do |format|
      if @comment.save
        @article = Article.find_by_id(params[:comment][:article_id])
        @comments = @article.comments.order("created_at") if @article.present?
        format.html { redirect_to @comment, notice: 'Comment was successfully created.' }
        format.json { render :show, status: :created, location: @comment }
        format.js{}
      else
        format.html { render :new }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
        format.js{}
      end
    end
  end

  #def update
   # @comment.update(comment_params)
   # respond_with(@comment)
  #end

  def destroy
    @comment.destroy
    respond_to do |format|
      @article = Article.find_by_id(params[:article_id])
      @comments = @article.comments.order("created_at") if @article.present?
      format.html { redirect_to comments_url, notice: 'Comment was successfully destroyed.' }
      format.json { head :no_content }
      format.js {}
    end
  end

  private
    def set_comment
      @comment = Comment.find(params[:id])
    end

    def comment_params
       params.require(:comment).permit(:body,:article_id,:parent_id)
    end
end
