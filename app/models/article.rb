class Article < ActiveRecord::Base
  belongs_to :magazine
  has_many :comments
  validates :title ,:presence => { :message => "can not be blank." },length: { in: 1..30 }
  #validates :author ,:presence => { :message => "can not be blank." },length: { in: 1..30 }
end
