class Magazine < ActiveRecord::Base
  belongs_to :user
  has_many :articles
  validates :title ,:presence => { :message => "Please Enter the title." },length: { in: 1..50 }
end
